package io.niceday.sample.api.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @since       2018.10.02
 * @author      lucas
 * @description sample
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity(name="sample")
@Description("샘플")
public class Sample extends Base {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Description("샘플일련번호")
	@Column(nullable=false, precision=20)
	private Long id;

	@Description("사용자아이디")
	@Column(nullable=false, length=50)
	private String userId;

	@Description("제목")
	@Column(length=100)
	private String title;

	@Description("내용")
	@Column(length=500)
	private String content;

	@Description("등록일시")
	@CreatedDate
	@Column
	private LocalDateTime createdAt;
}